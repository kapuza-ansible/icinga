# Icinga server install on Ubuntu16
## Install
### hosts setup
```bash
echo -e '[icinga]\nicingahost\n' >> hosts
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible icinga -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt"
```

### Download ansible role
```bash
mkdir ./roles
cat <<'EOF' > ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/icinga.git
  name: icinga
EOF
echo "roles/icinga" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## Use role
```bash
cat << 'EOF' > icinga_server.yml
---
- name: Install icinga server
  hosts:
    icinga
  tasks:
    - name: Icinga server install
      include_role:
        name: icinga
      vars:
        icinga_action: 'server_install'
        icinga_server:
          hashed_passwd: '$apr1$T/AtuXhI$qM76I9k9AbuyPFv8boIF2.'
EOF

cat << 'EOF' > icinga_agent.yml
---
- name: Install icinga server
  hosts:
    icinga
  tasks:
    - name: Icinga server install
      include_role:
        name: icinga
      vars:
        icinga_action: 'agent_install'
        icinga_agent:
          server_port: 5666
          allowed_hosts: "127.0.0.1,10.121.0.0/24,192.168.120.0/24"
EOF

ansible-playbook icinga_server.yml
```
