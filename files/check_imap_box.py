#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# -*- Mode: Python -*-
# Check IMAP box for number of letters in INBOX folder.
# Aleksandrov Artyom 2011 <a.aleksandrov@westcall.spb.ru>
# version 0.1

import sys, os, imaplib
from optparse import OptionParser

p = OptionParser()
p.add_option("-H","--host", dest="host", default='localhost', help="Hostname or IP address of IMAP server (default localhost)")
p.add_option("-u","--user", dest="user", help="Username")
p.add_option("-p","--password", dest="password", help="Password")
p.add_option("-s","--ssl", dest="ssl", type="int", default=0, help="Use or not SSL (default=0)")
p.add_option("-P","--port", type="int", dest="port", default='443', help="Port for IMAP (default 443)")
p.add_option("-c","--critical", type="int", default='10', dest="crit", help="Critical value (default 10)")
p.add_option("-w","--warning", type="int", dest="warn", default='5', help="Warning value (default 5)")
(opts, args) = p.parse_args()
#print opts
if opts.crit < opts.warn:
	print "Critical value < Warning value. Check your config"
	sys.exit(1)
if not opts.user or not opts.password:
	print "\nUsage: %s -u <username> -p <password> [<options>]\n"%sys.argv[0]
	print "For full usage instructions please invoke with -h option\n"
	sys.exit(3)
try:
	if opts.ssl == 1:
		server = imaplib.IMAP4_SSL(host=opts.host, port=opts.port)
	else:
		server = imaplib.IMAP4(host=opts.host, port=opts.port)
	server.login(opts.user, opts.password)
except Exception, e:
	print "CRITICAL: IMAP Login not Successful: %s" % e
	sys.exit(2)

count = int(server.select("INBOX")[1][0])
server.logout()
msg = 'INBOX folder has %s msg(s)' % (count)
if count > opts.crit:
	print 'CRITICAL ' + msg 
	sys.exit(2)
elif count > opts.warn:
	print 'WARNING ' + msg
	sys.exit(1)
elif count <= opts.warn:
	print 'OK ' + msg
        sys.exit(0)
else:
	print 'UNKNOWN'
        sys.exit(3)
