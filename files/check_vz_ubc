#!/usr/bin/perl -w
# Copyright (c) 2012, Evgeny Arhipov
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use strict;
#use Data::Dumper;
use subs qw(exit_critical exit_warning exit_ok usage);
use Getopt::Long;

my %alert;
my $critical_threshold = 95;
my $warning_threshold = 90;
my $time_threshold = 3600; # an hour
my $alert_output;
my $mode = 'host';
my $logfile = '/var/lib/nagios/vz_resources.db';

my $perfdata;
my @event_log = ();
my @current;
GetOptions(
    'h|help' => sub { &usage; exit 1 },
    'w|warning=s' => \$warning_threshold,
    'c|critical=s' => \$critical_threshold,
    't|time=s' => \$time_threshold,
    'm|mode=s' => \$mode,
);
$warning_threshold = $warning_threshold / 100;
$critical_threshold = $critical_threshold / 100;

# Open history file, if it exists
if(open LOG, $logfile){
    @event_log = <LOG>;
    close LOG;
}

open my $resource_file, $mode eq 'guest' ? '/proc/user_beancounters' : '/proc/bc/resources'
    or exit_critical "unable to access resource counters";

my @containers = split /(?=\s[0-9]{1,4}\:\s)/, do { local $/; <$resource_file> };
close $resource_file;
# Version string
shift @containers;
my $time = time;

for (@containers){
    # Get CTID from first string and chop it off
    # 0:  kmemsize 23031189 48070656 9223372036854775807 9223372036854775807 0
    my $ctid = $1 if s/^\s([0-9]+)://;
    $perfdata = '';
    # Parse each resource row
    RESOURCE: for (split /\n/){
	#    lockedpages 0 3558  9223372036854775807 9223372036854775807 0
	if( /^\s+([a-zA-Z]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s*$/m ){
	    my ($resource, $held, $maxheld, $limit, $barrier, $failcount) = ($1, $2, $3, $4, $5, $6);
	    next RESOURCE if $resource =~ /^dummy$/;
	    # Correct unit representation is important for perfdata output
	    # and can save a lot of space
	    my $units = '';
	    my $factor = 1;
	    if($resource =~ m/buf$|size$/){
		# Bytes
		$units = 'B';
		$factor = 1;
	    }elsif($resource =~ m/pages$/){
		# Page is usually 4kb
		$units = "B";
		$factor = 4096;
	    }
	    my $h_limit = ( $limit == 9223372036854775807 || $limit == 2147483647 || $limit == 0 ) ? 'unlim' : $limit;
#	    $perfdata .= sprintf "'%s'=%.1f%s;%s;%s ", $resource, $held * $factor, $units, 
#	    $h_limit eq 'unlim' ? '' : ( $limit * $warning_threshold ) * $factor , $h_limit eq 'unlim' ? '' : ( $limit * $critical_threshold ) * $factor 
	    $perfdata .= sprintf "'%s'=%s%s;; ", $resource, $held * $factor, $units
	    if $mode eq 'guest';

	    # We have problems: nonzero failcount or held amount approaching critical
	    # Check resource usage
	    if ( $limit != 0 and ($held / $limit >= $critical_threshold) ){
		$alert{critical}{$ctid} .= sprintf "%s: %s usage is %s%s/%s%s (%.1f%%), fc=%s; ", 
		$ctid, $resource, $held * $factor , $units, $limit * $factor , $units, ( ($held / $limit) * 100 ), $failcount;
		push @current, "$ctid;$time;$resource;$held;$maxheld;$limit;$barrier;$failcount";
		next RESOURCE;
	    } 

	    if ( $limit != 0 and ($held / $limit >= $warning_threshold) ){
		$alert{warning}{$ctid} .= sprintf "%s: %s usage is %s%s/%s%s (%.1f%%), fc=%s; ", 
		$ctid, $resource, $held * $factor , $units, $limit * $factor , $units, ( ($held / $limit) * 100 ), $failcount;
	    } 

	    if ( $failcount != 0 ){
		# If we are here, resource usage is acceptable, but has nonzero failcount.
		# Is this a new problem or a known one?
		for (@event_log){
		    if (/^$ctid;([0-9]+);$resource;[0-9]+;[0-9]+;[0-9]+;[0-9]+;([0-9]+)$/){
			if ( $time - $1 <= $time_threshold ) {
			    # We have a known problem here; save the record and raise alert.
			    push @current, $_;
			    $alert{critical}{$ctid} .= sprintf "%s: %s overlimit, h=%s%s,m=%s,fc=%s; ", $ctid, $resource, $held * $factor, $units, 
			    ($h_limit eq 'unlim' ? 'unlim' : $limit * $factor . $units ), $failcount;
			    next RESOURCE;
			} elsif ($time - $1 > $time_threshold) {
			    # We have a known problem here; just save the record.
			    push @current, $_;
			    next RESOURCE;			    
			} elsif ($failcount > $2) {
			    # Update log entry if the problem is growing, so it will not go out of date. Raise alert.
			    push @current, "$ctid;$time;$resource;$held;$maxheld;$limit;$barrier;$failcount";
			    $alert{critical}{$ctid} .= sprintf "%s: %s overlimit, h=%s%s,m=%s,fc=%s; ", $ctid, $resource, $held * $factor, $units, 
			    ($h_limit eq 'unlim' ? 'unlim' : $limit * $factor . $units ), $failcount;
			    next RESOURCE;			    
			}
		    }
		} # end history lookup
		# history lookup didnt succeed, so we create a new record and raise alert
		$alert{critical}{$ctid} .= sprintf "%s: %s overlimit, h=%s%s,m=%s,fc=%s; ", $ctid, $resource, $held * $factor, $units, 
		($h_limit eq 'unlim' ? 'unlim' : $limit * $factor . $units ), $failcount;
		push @current, "$ctid;$time;$resource;$held;$maxheld;$limit;$barrier;$failcount";
	    }# end if failcount 
	}# end matching regex
    }#end row split
}# end containers
open LOG, '>', $logfile or exit_critical "unable to write history file";

for (@current){
    chomp; print LOG $_ . "\n";
}

foreach my $type (keys %alert){
    foreach my $ctid (keys %{ $alert{$type} }){
	$alert_output .= $alert{$type}{$ctid};
    }
}

exit_critical $alert_output, $perfdata if $alert{critical};
exit_warning $alert_output, $perfdata if $alert{warning};
exit_ok "all parameters are within acceptable ranges", $perfdata;

sub exit_critical {
    my ($message, $perfdata) = @_;
    print STDOUT "CRITICAL: $message" . ( $perfdata ? " | $perfdata" : '' ) . "\n";
    exit 2;
}
sub exit_warning {
    my ($message, $perfdata) = @_;
    print STDOUT "WARNING: $message" . ( $perfdata ? " | $perfdata" : '' ) . "\n";
    exit 1;
}
sub exit_ok {
    my ($message, $perfdata) = @_;
    print STDOUT "OK: $message" . ( $perfdata ? " | $perfdata" : '' ) . "\n";
    exit 0;
}

sub usage {
    print STDERR <<END
 OpenVZ UBC checker plugin v.1.0 is written by 
 Eugene Arhipov (ae at arhv.net) under BSD licence.

 Usage:
 $0 [ -m <host|guest> ] [ -w <0..100> ] [ -c <0..100> ] [ -t <time in seconds> ] | -h

 Options:
 -t, --time [integer]    This parameter controls how the script should 
			 treat 'failcount' field. 
			 the script will raise critical level alerts
			 for the duration of this time period (starting 
			 with the time it was first seen).
			 When this timeout expies, and if the problem
			 is not growing (failcount is not increasing)
			 the alert will cease.
			 State will get back to 'critical' even after
			 the expiration if 'failcount' field increases.

 -m, --mode [host|guest] when operating in host mode (default) the
			 program scans entire resource file
			 (/proc/bc/resources), but doesnt give
			 performance data due to the limitations of 
			 NRPE protocol;
			 in guest mode it gives perfdata for the 
			 last container and scans different file
			 (/proc/user_beancounters), which is present
			 inside the guest.

 -w, --warning [1..100]  max usage of any resource before it raises
			 warning level alert (default $warning_threshold)

 -c, --critical [1..100] max usage of any resource before it raises
			 critical level alert (default $critical_threshold)
 -h, --help              print this text

 All of above parameters are optional.

 Examples:
 $0 -w 90 -c 95 -t 3600 -m host
 Scan for excessive resource usage, also look for 'failcount' events and remember
 their state, raising alert for the duration of one hour (3600 seconds)

 Files:
 The script uses resource files to retrive counter data (see '-m' above)
 and logfile ($logfile) to save 'failcount' states.

END
}
